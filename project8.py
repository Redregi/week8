addLibPath("/Users/jchurchill2016/Documents/Python_Basics/Week8/")
from triviaEdited import *


def qOne():
  qdOne = open("/Users/jchurchill2016/Documents/Python_Basics/Week8/qOne.txt", 'r')
  #contents = qdOne.read()
  lines = qdOne.readlines()
  #qdOne.rstrip("\n")
  nC = 0
  print("Player One, start!")
  questionOne = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qOtQ = lines[0]
  qOp1 = lines[1]
  qOp2 = lines[2]
  qOp3 = lines[3]
  qOp4 = lines[4]
  qOnC = "D"
  
  questionOne.set_tQ(qOtQ)
  questionOne.set_p1(qOp1)
  questionOne.set_p2(qOp2)
  questionOne.set_p3(qOp3)
  questionOne.set_p4(qOp4)
  questionOne.set_nC(qOnC)
  
  print(questionOne.get_tQ())
  print("A. ", questionOne.get_p1())
  print("B. ", questionOne.get_p2())
  print("C. ", questionOne.get_p3())
  print("D. ", questionOne.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qOnC):
    nC += 1
    
  questionTwo = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTtQ = lines[5]
  qTp1 = lines[6]
  qTp2 = lines[7]
  qTp3 = lines[8]
  qTp4 = lines[9]
  qTnC = "B"
  
  questionTwo.set_tQ(qTtQ)
  questionTwo.set_p1(qTp1)
  questionTwo.set_p2(qTp2)
  questionTwo.set_p3(qTp3)
  questionTwo.set_p4(qTp4)
  questionTwo.set_nC(qTnC)
  
  print(questionTwo.get_tQ())
  print("A. ", questionTwo.get_p1())
  print("B. ", questionTwo.get_p2())
  print("C. ", questionTwo.get_p3())
  print("D. ", questionTwo.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTnC):
    nC += 1
  
  questionThree = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTHtQ = lines[10]
  qTHp1 = lines[11]
  qTHp2 = lines[12]
  qTHp3 = lines[13]
  qTHp4 = lines[14]
  qTHnC = "A"
  
  questionThree.set_tQ(qTHtQ)
  questionThree.set_p1(qTHp1)
  questionThree.set_p2(qTHp2)
  questionThree.set_p3(qTHp3)
  questionThree.set_p4(qTHp4)
  questionThree.set_nC(qTHnC)
  
  print(questionThree.get_tQ())
  print("A. ", questionThree.get_p1())
  print("B. ", questionThree.get_p2())
  print("C. ", questionThree.get_p3())
  print("D. ", questionThree.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTHnC):
    nC += 1
  
  questionFour = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFtQ = lines[15]
  qFp1 = lines[16]
  qFp2 = lines[17]
  qFp3 = lines[18]
  qFp4 = lines[19]
  qFnC = "C"
  
  questionFour.set_tQ(qFtQ)
  questionFour.set_p1(qFp1)
  questionFour.set_p2(qFp2)
  questionFour.set_p3(qFp3)
  questionFour.set_p4(qFp4)
  questionFour.set_nC(qFnC)
  
  print(questionFour.get_tQ())
  print("A. ", questionFour.get_p1())
  print("B. ", questionFour.get_p2())
  print("C. ", questionFour.get_p3())
  print("D. ", questionFour.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFnC):
    nC += 1

  questionFive = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFItQ = lines[20]
  qFIp1 = lines[21]
  qFIp2 = lines[22]
  qFIp3 = lines[23]
  qFIp4 = lines[24]
  qFInC = "D"
  
  questionFive.set_tQ(qFItQ)
  questionFive.set_p1(qFIp1)
  questionFive.set_p2(qFIp2)
  questionFive.set_p3(qFIp3)
  questionFive.set_p4(qFIp4)
  questionFive.set_nC(qFInC)
  
  print(questionFive.get_tQ())
  print("A. ", questionFive.get_p1())
  print("B. ", questionFive.get_p2())
  print("C. ", questionFive.get_p3())
  print("D. ", questionFive.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFInC):
    nC += 1
  print("Congratulations! Player One got ", nC, " correct!")

  

def qTwo():
  qdTwo = open("/Users/jchurchill2016/Documents/Python_Basics/Week8/qTwo.txt", 'r')
  nC = 0
  line = qdTwo.readlines()
  print("Player Two, start!")
  questionOne = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qOtQ = line[0]
  qOp1 = line[1]
  qOp2 = line[2]
  qOp3 = line[3]
  qOp4 = line[4]
  qOnC = "D"
  
  questionOne.set_tQ(qOtQ)
  questionOne.set_p1(qOp1)
  questionOne.set_p2(qOp2)
  questionOne.set_p3(qOp3)
  questionOne.set_p4(qOp4)
  questionOne.set_nC(qOnC)
  
  print(questionOne.get_tQ())
  print("A. ", questionOne.get_p1())
  print("B. ", questionOne.get_p2())
  print("C. ", questionOne.get_p3())
  print("D. ", questionOne.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qOnC):
    nC += 1
    
  questionTwo = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTtQ = line[5]
  qTp1 = line[6]
  qTp2 = line[7]
  qTp3 = line[8]
  qTp4 = line[9]
  qTnC = "B"
  
  questionTwo.set_tQ(qTtQ)
  questionTwo.set_p1(qTp1)
  questionTwo.set_p2(qTp2)
  questionTwo.set_p3(qTp3)
  questionTwo.set_p4(qTp4)
  questionTwo.set_nC(qTnC)
  
  print(questionTwo.get_tQ())
  print("A. ", questionTwo.get_p1())
  print("B. ", questionTwo.get_p2())
  print("C. ", questionTwo.get_p3())
  print("D. ", questionTwo.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTnC):
    nC += 1
  
  questionThree = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTHtQ = line[10]
  qTHp1 = line[11]
  qTHp2 = line[12]
  qTHp3 = line[13]
  qTHp4 = line[14]
  qTHnC = "A"
  
  questionThree.set_tQ(qTHtQ)
  questionThree.set_p1(qTHp1)
  questionThree.set_p2(qTHp2)
  questionThree.set_p3(qTHp3)
  questionThree.set_p4(qTHp4)
  questionThree.set_nC(qTHnC)
  
  print(questionThree.get_tQ())
  print("A. ", questionThree.get_p1())
  print("B. ", questionThree.get_p2())
  print("C. ", questionThree.get_p3())
  print("D. ", questionThree.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTHnC):
    nC += 1
  
  questionFour = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFtQ = line[15]
  qFp1 = line[16]
  qFp2 = line[17]
  qFp3 = line[18]
  qFp4 = line[19]
  qFnC = "C"
  
  questionFour.set_tQ(qFtQ)
  questionFour.set_p1(qFp1)
  questionFour.set_p2(qFp2)
  questionFour.set_p3(qFp3)
  questionFour.set_p4(qFp4)
  questionFour.set_nC(qFnC)
  
  print(questionFour.get_tQ())
  print("A. ", questionFour.get_p1())
  print("B. ", questionFour.get_p2())
  print("C. ", questionFour.get_p3())
  print("D. ", questionFour.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFnC):
    nC += 1

  questionFive = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFItQ = line[20]
  qFIp1 = line[21]
  qFIp2 = line[22]
  qFIp3 = line[23]
  qFIp4 = line[24]
  qFInC = "D"
  
  questionFive.set_tQ(qFItQ)
  questionFive.set_p1(qFIp1)
  questionFive.set_p2(qFIp2)
  questionFive.set_p3(qFIp3)
  questionFive.set_p4(qFIp4)
  questionFive.set_nC(qFInC)
  
  print(questionFive.get_tQ())
  print("A. ", questionFive.get_p1())
  print("B. ", questionFive.get_p2())
  print("C. ", questionFive.get_p3())
  print("D. ", questionFive.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFInC):
    nC += 1
  print("Congratulations! Player Two got ", nC, " correct!")

  

  
    
  
    
    
    
    
    


qOne()
qTwo()


  
  
  