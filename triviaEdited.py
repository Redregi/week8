class Trivia:

  def __init__(self, tQ, p1, p2, p3, p4, nC):
    self.__tQ = tQ
    self.__p1 = p1
    self.__p2 = p2
    self.__p3 = p3
    self.__p4 = p4
    self.__nC = int(nC)
  
  def get_tQ(self):
    return self.__tQ
  
  def get_p1(self):
    return self.__p1
  
  def get_p2(self):
    return self.__p2
  
  def get_p3(self):
    return self.__p3
  
  def get_p4(self):
    return self.__p4
  
  def get_nC(self):
    return self.__nC
  
  def set_tQ(self, tQ):
    self.__tQ = tQ

  def set_p1(self, p1):
    self.__p1 = p1

  def set_p2(self, p2):
    self.__p2 = p2
    
  def set_p3(self, p3):
    self.__p3 = p3

  def set_p4(self, p4):
    self.__p4 = p4

  def set_nC(self, nC):
    self.__nC = nC  
  
  def add_nC(self):
    self.__nC = self.__nC + 1
  
  
  
  
  
  
  